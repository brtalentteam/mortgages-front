import Ember from 'ember';

export default Ember.Component.extend({
  className: 'slider',
  didInsertElement() {
    Ember.$('.slider').slider();
  }
});
