import Ember from 'ember';

export default Ember.Controller.extend({
  isShowingForm: true,
  actions: {
    toggleForm() {
      this.toggleProperty('isShowingForm');
    }
  }
});
