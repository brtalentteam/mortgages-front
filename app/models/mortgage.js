import DS from 'ember-data';

export default DS.Model.extend({
  householdIncome: DS.attr('number'),
  debtPayments: DS.attr('number'),
  propertyTaxes: DS.attr('number'),
  condoFees: DS.attr('number'),
  heatingCosts: DS.attr('number'),
  downPayment: DS.attr('number'),
  interestRate: DS.attr('number'),
  amortization: DS.attr('number'),

  maxMortgage: DS.attr('number'),
  maxHousePrice: DS.attr('number'),
  maxMonthlyMortgage: DS.attr('number')
});
